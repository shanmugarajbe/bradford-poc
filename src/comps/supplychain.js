import React, { Fragment } from 'react';

import Breadcrumb from '../helpers/breadcrumb';
import SideBar from '../helpers/sidebar';

class SupplyChain extends React.Component {
  render() {
    return (
        <Fragment>
            <Breadcrumb {...this.props} />
        <div className="contentComp">
            <div id="content">
                <div className="title">
                    <div className="heading">Supply Chain</div>
                </div>
                <SideBar />
                <div id="mainColumn">
                    <div className="privacyPolicy">
                        <div className="contentBox alt">
                            <div className="sleeve">
                                <p>Bradford strives to enrich the lives of its customers, employees and Supply Chain partners.  We aim to develop and market a variety of inspired, unique, and highly valued programs, products and services that resonate with their interests and passions.  All the while, we recognize the challenge of the workplace environment.</p>                    
                                <div className="title">Commitment</div>
                                <p>Bradford is committed to the identification and elimination of all types of forced labor and human-trafficking within its supply chain providers.  We are committed to improving labor practices, and we endeavor to achieve fair, safe and healthy work-place conditions not only in our Supply Chain, but in our worldwide corporate offices.</p>                    
                                <div className="title">Supply Chain Verification</div>
                                <p>Bradford utilizes a Factory Profile tool to evaluate potential factories.  The Factory Profile tool and on-site visits by Bradford employees, together with engagement of third party auditing, is mandatory for acceptance of a factory into our Supply Chain.  Continued monitoring to ensure adherence to the Bradford Code of Conduct is mandatory.</p>                    
                                <div className="title">Continued Monitoring</div>
                                <p>Once a factory is approved to the Bradford Supply Chain, the factory commits to:</p>
                                <ul>
                                    <li>On-going monitoring by Bradford employees and third party auditors;</li>
                                    <li>An on-going commitment to improve conditions and internal systems to support the Code of Conduct; and </li>
                                    <li>Resolve any identified gaps between workplace standards and the Code of Conduct.</li>
                                </ul>
                                <div className="title">Training</div>
                                <p>Bradford's on-going training is built on the learning's from previous years.</p>                    
                            </div>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
        </Fragment>
    )
  }
}

export default SupplyChain;