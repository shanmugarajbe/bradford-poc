import React, { Fragment } from 'react';

import Breadcrumb from '../helpers/breadcrumb';
import SideBar from '../helpers/sidebar';

class HomePage extends React.Component {
  render() {
    return (
        <Fragment>
            <Breadcrumb {...this.props} />
        <div className="contentComp">
            <div id="content">
                <div className="title">
                    <div className="heading">Home Page</div>
                </div>
                <SideBar />
                <div id="mainColumn">
                    <div className="privacyPolicy">
                        <div className="alt">
                                <p>Welcome to Bradford Home Page</p>                    
                        </div>
                    </div>
                </div>     
            </div>
        </div>
        </Fragment>
    )
  }
}

export default HomePage;

// then save as > Post.js with file updated accordingly