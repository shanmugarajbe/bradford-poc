import React, { Component } from 'react';
import {  Switch, Route, Link } from 'react-router-dom';

import SupplyChain from '../comps/supplychain';
import HomePage from '../comps/homePage';
import Footer from '../helpers/footer';
import logo from '../images/logo-bradford-exchange-exclusives-280.png';

export default class Layout extends Component {

    render() {
        return (
            <div id="wrapper" className="App">
                <header className="App-header">
                    <div className="logo"><img src={logo} alt="BradFord" /></div>
                    <div className="navigation">
                        <span><Link to="/">Home</Link></span>
                        <span><Link to="/supplychain/">Supply Chain</Link></span>
                    </div>                
                </header>
                <Switch>
                   
                    <Route path="/supplychain" component={SupplyChain} />
                    <Route path="/" component={HomePage} />
                </Switch>
                <Footer />
            </div>
        )
    }
}