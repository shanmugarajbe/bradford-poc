import React, { Component } from 'react';

export default class BreadCrumb extends Component {
    constructor(props) {
        super(props);
        if(this.props.match && this.props.match.url.includes('supplychain')) {
            this.state = { breadCrumb: "/Supply Chain" }
        } else {
            this.state = { breadCrumb: this.props.match.url }
        }
    }
    render() {
        return (
            <div>
                <div id="breadcrumbs">
                    <div className="label">You Are Here: </div>
                    <ul>
                    <li><h2><a href="/" >The Bradford Exchange Online</a></h2></li>
                    <li className="last"><h1>{this.state.breadCrumb}</h1></li>
                    </ul>
                </div>             
                <div className="clear"></div>
            </div>
        )
    }
}