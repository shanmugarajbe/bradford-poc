import React, { Component } from 'react';

export default class BreadCrumb extends Component {
    render() {
        return (
            <div id="sidebar">
                    <div className="contentSlotSidebar">
                        <div className="contentBox alt">
                            <div className="sleeve">
                                <div className="innerBox">
                                    <div className="heading2">Top Categories</div>					
                                    <ul className="linkList">
                                        <li><a href="#" tabIndex="-1">Jewelry &amp; Watches</a></li>
                                        <li><a href="#" tabIndex="-1">Apparel &amp; Accessories</a></li>
                                        <li><a href="#" tabIndex="-1">Dolls</a></li>
                                        <li><a href="#" tabIndex="-1">Coins</a></li>
                                        <li><a href="#" tabIndex="-1">Sports</a></li>
                                        <li><a href="#" tabIndex="-1">Thomas Kinkade</a></li>
                                        <li><a href="#" tabIndex="-1">Disney</a></li>
                                    </ul>
                                    <div className="more"><a href="#" tabIndex="-1">Shop by Theme</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}