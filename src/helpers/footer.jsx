import React, { Component, Fragment } from 'react';

import footerlogo from '../images/logo-bradford-exchange-exclusives-200.png';

export default class Footer extends Component {
    render() {
        return (
            <Fragment>
                <div id="innerFooter">
                    <div className="leftBrand"><a href="/"><img className="logo" src={footerlogo} width="200px" height="58px" alt="The Bradford Exchange" /></a></div>
                    <div className="miniCart"><div className="itemCount">0 Items in Cart</div></div>
                </div>
                <div id="globalFooter">
                <p className="copyright" >Copyright ©2018 The Bradford Exchange. All rights reserved.</p>
                </div>
            </Fragment>
        )
    }
}