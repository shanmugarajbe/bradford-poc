import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';


import Breadcrumb from './helpers/breadcrumb';
import Layout from './layout/layout';


import './App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div id="container">
            <Layout {...this.props} />
        </div>
        
      </BrowserRouter>
    );
  }
}

export default App;
